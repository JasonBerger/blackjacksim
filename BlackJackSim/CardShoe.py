#!/usr/bin/env python3

import random

deck = ['AC', '2C', '3C', '4C', '5C', '6C', '7C', '8C','9C', 'TC', 'JC', 'QC', 'KC',
        'AD', '2D', '3D', '4D', '5D', '6D', '7D', '8D','9D', 'TD', 'JD', 'QD', 'KD',
        'AH', '2H', '3H', '4H', '5H', '6H', '7H', '8H','9H', 'TH', 'JH', 'QH', 'KH',
        'AS', '2S', '3S', '4S', '5S', '6S', '7S', '8S','9S', 'TS', 'JS', 'QS', 'KS']

blackjack_deck = [  'AC', '2C', '3C', '4C', '5C', '6C', '7C', '8C','9C', 'TC', 'TC', 'TC', 'TC',
                    'AD', '2D', '3D', '4D', '5D', '6D', '7D', '8D','9D', 'TD', 'TD', 'TD', 'TD',
                    'AH', '2H', '3H', '4H', '5H', '6H', '7H', '8H','9H', 'TH', 'TH', 'TH', 'TH',
                    'AS', '2S', '3S', '4S', '5S', '6S', '7S', '8S','9S', 'TS', 'TS', 'TS', 'TS']



class CardShoe:
    """A card shoe is a container for a number of decks of cards."""
    def __init__(self, num_decks=6, penetration=0.75):
        self.num_decks = num_decks
        self.cards = []
        self.cutCard = 0
        self.penetration = penetration
        self.shuffleReady = False
        self.runningCount = 0
        self.blackjackmode = False
        self.discardCount =0
        self.discardCards = []
        self.reset()

    def reset(self, num_decks=None):
        """Reset the card shoe to contain the specified number of decks."""

        if num_decks is not None:
            self.num_decks = num_decks


        if self.blackjackmode:
            self.cards = blackjack_deck * self.num_decks
        else:
            self.cards = deck * self.num_decks

        if(self.discardCount != self.runningCount):
            print("discard count: {0} running count: {1}".format(self.discardCount,self.runningCount))
            raise Exception("discard count and running count do not match")

        self.cutCard = len(self.cards) - ( self.penetration * len(self.cards))
        # print("\n********resetting shoe********")
        # print("cut card: {0}\n\n".format(self.cutCard))
        self.runningCount = 0
        self.discardCount = 0
        self.shuffleReady = False
        self.discardCards = []
        self.shuffle()

    def shuffle(self):
        """Shuffle the cards in the shoe."""
        random.shuffle(self.cards)
    
    def getTrueCount(self, roundToInt=True):

        if roundToInt:
            return  round(self.runningCount / ((len(self.cards) / 52)))
        else:
            return self.runningCount / ((len(self.cards) / 52))
        
        
    def deal(self, faceup=True):
        """Remove and return the top card from the shoe."""
        card = self.cards.pop()
        #print("dealing card: {0}".format(len(self.cards)))
        
        if len(self.cards) < self.cutCard and not self.shuffleReady:
            self.shuffleReady = True
            #print("cut card hit")

        #adjust running count

        if faceup :
            if card[0] in ['2','3','4','5','6']:
                self.runningCount += 1
            elif card[0] in ['7','8','9']:
                pass
            else:
                self.runningCount -= 1


        return card

    def discard(self, card):
        """Discard the specified card."""

        if card[0] in ['2','3','4','5','6']:
            self.discardCount += 1
        elif card[0] in ['7','8','9']:
            pass
        else:
            self.discardCount -= 1
        
        self.discardCards.append(card)
    
    def __len__(self):
        """Return the number of cards remaining in the shoe."""
        return len(self.cards)
    
    def __str__(self):
        """Return a string representation of the card shoe."""
        return ', '.join([str(card) for card in self.cards])