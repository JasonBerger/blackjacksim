#!/usr/bin/env python3

from BlackJackSim.CardShoe import CardShoe


valueDict = {'A': 11, '2': 2, '3': 3, '4': 4, '5': 5,
             '6': 6, '7': 7, '8': 8, '9': 9, 'T': 10,
             'J': 10, 'Q': 10, 'K': 10}


default_strategy_map = {
    #Dealer        2   3   4  5   6   7   8   9   T    A

    #Hard Values
    '5':     [    'H','H','H','H','H','H','H','H','H','H'],
    '6':     [    'H','H','H','H','H','H','H','H','H','H'],
    '7':     [    'H','H','H','H','H','H','H','H','H','H'],
    '8':     [    'H','H','H','H','H','H','H','H','H','H'],
    '9':     [    'H','DH','DH','DH','DH','H','H','H','H','H'],
    '10':    [    'DH','DH','DH','DH','DH','DH','DH','DH','H','H'],
    '11':    [    'DH','DH','DH','DH','DH','DH','DH','DH','DH','H'],
    '12':    [    'H','H','S','S','S','H','H','H','H','H'],
    '13':    [    'S','S','S','S','S','H','H','H','H','H'],
    '14':    [    'S','S','S','S','S','H','H','H','H','H'],
    '15':    [    'S','S','S','S','S','H','H','H','H','H'],
    '16':    [    'S','S','S','S','S','H','H','H','H','H'],
    '17':    [    'S','S','S','S','S','S','S','S','S','S'],
    '18':    [    'S','S','S','S','S','S','S','S','S','S'],
    '19':    [    'S','S','S','S','S','S','S','S','S','S'],
    '20':    [    'S','S','S','S','S','S','S','S','S','S'],
    '21':    [    'S','S','S','S','S','S','S','S','S','S'],
    #Soft Values
    'S13':    [    'H','H','H','DH','DH','H','H','H','H','H'],
    'S14':    [    'H','H','H','DH','DH','H','H','H','H','H'],
    'S15':    [    'H','H','DH','DH','DH','H','H','H','H','H'],
    'S16':    [    'H','H','DH','DH','DH','H','H','H','H','H'],
    'S17':    [    'H','DH','DH','DH','DH','H','H','H','H','H'],
    'S18':    [    'S','DS','DS','DS','DS','S','S','H','H','H'],
    'S19':    [    'S','S','S','S','S','S','S','S','S','S'],
    'S20':    [    'S','S','S','S','S','S','S','S','S','S'],
    'S21':    [    'S','S','S','S','S','S','S','S','S','S'],
    #Pairs
    '22':    [    'P','P','P','P','P','P','H','H','H','H'],
    '33':    [    'P','P','P','P','P','P','H','H','H','H'],
    '44':    [    'H','H','P','P','P','H','H','H','H','H'],
    '55':    [    'D','D','D','D','D','D','D','D','H','H'],
    '66':    [    'P','P','P','P','P','H','H','H','H','H'],
    '77':    [    'P','P','P','P','P','P','H','H','H','H'],
    '88':    [    'P','P','P','P','P','P','P','P','P','P'],
    '99':    [    'P','P','P','P','P','S','P','P','S','S'],
    'TT':    [    'S','S','S','S','S','S','S','S','S','S'],
    'AA':    [    'P','P','P','P','P','P','P','P','P','P']
}


class BlackJackAdjustment:
    def __init__(self):
        self.hands = 0
        self.bet = 0
        rules = {}
        

class BlackJackStrategy:
    def __init__(self, player):
        self.ruleMap = default_strategy_map; 
        self.adjustments = {} 
        self.player = player
    
    def addAdjustment(self, truecount, hands, bet):
        if truecount not in self.adjustments:
            self.adjustments[truecount] = BlackJackAdjustment()
            self.adjustments[truecount].hands = hands
            self.adjustments[truecount].bet = bet

    def getBets(self):
        bets = []
        truecount = self.player.game.shoe.getTrueCount()
        if truecount in self.adjustments:
            handCount = self.adjustments[truecount].hands
            bet = self.adjustments[truecount].bet
            for i in range(handCount):
                bets.append(bet)
        else: 
            bets.append(self.player.game.minbet)
            
        
        
        # if(self.player.showHands):
        #     line = "Count: {0}/{1}, Bets: ".format(truecount, self.player.game.shoe.runningCount)
        #     for bet in bets:
        #         line += str(bet) + " "
        #     print(line)

        return bets

    def getAction(self, hand):

        return self.ruleMap[hand.getStratKey()][valueDict[hand.player.game.dealer.hands[0].cards[0][0]]-2]


class BlackJackHand:
    def __init__(self, player, bet):
        self.cards = []
        self.bet = bet
        self.value = 0
        self.done = False
        self.lost = False   #surrendered or busted
        self.player = player
        self.actions = []
        self.blackjack = False
        self.aceCount = 0
        self.line = ""

    def getStratKey(self):

        #initial hand
        if len(self.cards) == 2:

            hand = self.cards[0][0] + self.cards[1][0]

            if hand[0] == hand[1]:    #pair
                pass
            elif self.aceCount > 0:
                #reorder so A is first 
                hand = "S" + str(self.value)
            else:  
                hand = str(self.value)
        
        else: 

            if self.aceCount > 0:
                val = 0 
                ace_cusion = self.aceCount
                for card in self.cards:
                        val += valueDict[card[0]]

                while val > 21 and ace_cusion > 0:
                    val -= 10
                    ace_cusion -= 1


                self.value = val
                if(ace_cusion > 0):
                    hand = "S" + str(val)
                else:
                    hand = str(val)
            else:
                hand = str(self.value)

        
        

        # line = "hand: "
        # for card in self.cards:
        #     line += card + " "
        # print( line)
        # print( "hand: {0}".format(hand))

        return hand
    
    def giveCard(self, card):
        self.cards.append(card)
        if card[0] == 'A':
            self.aceCount += 1

        val = 0 
        ace_cusion = self.aceCount
        for card in self.cards:
                val += valueDict[card[0]]

        while val > 21 and ace_cusion > 0:
            val -= 10
            ace_cusion -= 1
        
        self.value = val
            
        if self.value > 21:
            self.done = True
            self.lost = True
        
        self.line += card + " "
        

    def takeCard(self, idx=-1):
        card = self.cards.pop(idx)
        if card[0] == 'A':
            self.aceCount -= 1

        val = 0 
        ace_cusion = self.aceCount
        for card in self.cards:
                val += valueDict[card[0]]

        while val > 21 and ace_cusion > 0:
            val -= 10
            ace_cusion -= 1

        self.value = val

        return card
    
    def stand(self):
        self.done = True
    
    def hit(self):
        self.giveCard(self.player.game.shoe.deal())
    
    def doubleDown(self):
        self.giveCard(self.player.game.shoe.deal())
        self.player.postBet(self.bet)
        self.player
        self.bet *= 2
        self.done = True
    
    def split(self):
        self.player.postBet(self.bet)                                       #pay for split
        self.player.hands.append(BlackJackHand(self.player, self.bet))      #add new hand to player
        self.player.hands[-1].giveCard(self.takeCard())                    #Give card to new hand
        # self.value = valueDict[self.cards[0][0]]                            #set value of this hand to remaining card

        self.line = self.cards[0] + " "                                     #set line of this hand to remaining card
        self.giveCard(self.player.game.shoe.deal())                         #deal new card to this hand
        self.player.hands[-1].giveCard(self.player.game.shoe.deal())        #deal new card to new hand
    
    def surrender(self):
        self.lost = True
        self.done = True
        self.player.stack += self.bet / 2


class BlackJackPlayer:
    def __init__(self, name, showHands=True):
        self.name = name
        self.stack = 0
        self.strategy = BlackJackStrategy(self) 
        self.game = None
        self.hands = []
        self.showHands = showHands
        self.av = 0
        self.totalBet = 0
        self.totalHands = 0
        self.handsLost = 0
        self.handsWon = 0
        self.handsPushed =0
    
    def resetHands(self):
        self.hands = []

    def printStats(self):

        print("\n*************************")
        print("Player: {0}".format(self.name))
        print("  Total Hands:     {0}".format(self.totalHands))
        print("  Hands Won:       {0}   ({1}%)".format(self.handsWon, round((self.handsWon / self.totalHands) * 100, 2)))
        print("  Hands Pushed:    {0}   ({1}%)".format(self.handsPushed, round((self.handsPushed / self.totalHands) * 100, 2)))
        print("  Hands Lost:      {0}   ({1}%)".format(self.handsLost, round((self.handsLost / self.totalHands) * 100, 2)))
        print("  AV:              {0}".format(self.av))
        print("  Calculated Edge: {0}%\n".format(round((self.av / self.totalBet) * 100, 2)))
        print("  Est Hours:       {0}".format(round(self.game.totalRounds / self.game.hph)))
        print("  Hrly Rate:       ${0}/hr\n".format(round(self.av/ (self.game.totalRounds / self.game.hph),2)))

    def postBet(self, bet):
        self.stack -=bet
        self.totalBet += bet 

    def postBets(self):
        bets = self.strategy.getBets()
        for bet in bets:
            self.postBet(bet)
            self.hands.append(BlackJackHand(self,bet))

    def loadStrategy(self, strategy):
        self.strategy = strategy
    
    def playHands(self):

        handIdx = 0

        while(handIdx < len(self.hands)):
            hand = self.hands[handIdx]

            while not hand.done:
                action = self.strategy.getAction(hand)

                hand.line += action + " "

                if action == 'H':
                    hand.hit()
                elif action == 'S':
                    hand.stand()
                elif action == 'D':
                    hand.doubleDown()
                elif action == 'P':
                    hand.split()
                elif action == 'DS':        #Double Down if possible, otherwise stand
                    if len(hand.cards) == 2:
                        hand.doubleDown()
                    else:
                        hand.stand()
                elif action == 'DH':        #doubledown if possible, otherwise hit
                    if len(hand.cards) == 2:
                        hand.doubleDown()
                    else:
                        hand.hit()
                else:
                    print("ERROR: invalid action")
                    exit(1)

                
                
                hand.actions.append(action)
            
            
            handIdx += 1


class StatBin:
    def __init__(self):
        self.total = 0
        self.win = 0
        self.lose = 0
        self.push = 0 
        self.av = 0
        self.blackjack = 0
        self.bets = 0
        self.dealerBust = 0

    def winPercent(self):
        return round((self.win * 100) / self.total,1)
    
    def losePercent(self):
        return round((self.lose * 100) / self.total,1)
    
    def pushPercent(self):
        return round((self.push * 100) / self.total,1)
    
    def blackjackPercent(self):
        return round((self.blackjack * 100) / self.total,1)
    
    def dealerBustPercent(self):
        return round((self.dealerBust * 100) / self.total,1)

    def edge(self):
        if(self.bets == 0):
            return 0

        return round((self.av / self.bets) * 100, 2)
    
    def printStats(self):
        line = "Total Hands:   {0}  \n".format( self.total,)
        line += "Hands Won:    {0}  ({1}%)\n".format(self.win, self.winPercent())
        line += "Hands Pushed: {0}  ({1}%)\n".format(self.push, self.pushPercent())
        line += "Hands Lost:   {0}  ({1}%)\n".format(self.push, self.losePercent())
        line += "Blackjack:    {0}  ({1}%)\n".format(self.blackjack, self.blackjackPercent())
        line += "Dealer Bust:  {0}  ({1}%)\n".format(self.dealerBust, self.dealerBustPercent())
        line += "AV:           {0}  \n".format(self.av)
        line += "Calc Edge:    {0}  \n\n".format(self.edge())

        print(line)





class BlackJackGame:
    def __init__(self, num_decks=6, penetration=0.75, blackjackpays=1.5, handsperhour=60): 
        

        self.players = []
        self.dealer = BlackJackPlayer("dealer")
        self.shoe = CardShoe(num_decks=num_decks, penetration=penetration)
        self.shoe.blackjackmode = True
        self.shoe.reset() 
        self.blackjackpays = blackjackpays
        self.hph = handsperhour 
        self.totalRounds =0
        self.minbet = 5

        self.countbins = {}
        self.upBins = {}
        
        #add in dealer rules 
    
    def addPlayer(self, player):
        player.game = self
        self.players.append(player)

    
    def playDealerHand(self):
        while self.dealer.hands[0].value < 17:
            self.dealer.hands[0].giveCard(self.shoe.deal())
    

    def getUpBin(self, upcard):

        if not upcard[0] in self.upBins:
            self.upBins[upcard[0]] = StatBin() 

        return self.upBins[upcard[0]]
    
    def getCountBin(self, truecount):
        
        if not truecount in self.countbins:
            self.countbins[truecount] = StatBin()
        

        return self.countbins[truecount]
    

    def printBins(self, min=-3, max=5):

        for x in ['2','3','4','5','6','7','8','9','T','A']:
            if x in self.upBins:
                print("**************\n Upcard: {0}".format(x))
                self.upBins[x].printStats()

        for i in range(min, max+1):
            if i in self.countbins:
                print("**************\n True Count: {0}".format(i))
                self.countbins[i].printStats()
    

    def playRound(self):
        #deal cards
        startingTrueCount = self.shoe.getTrueCount()
        handcount = 0
        self.totalRounds += 1

        #print("\nTC: {0}     RC: {1}    rem: {2}".format(self.shoe.getTrueCount(), self.shoe.runningCount, len(self.shoe)))


        #place bets
        for player in self.players:
            player.resetHands()
            player.postBets()


        #deal starting hands, order doesnt matter since there is no action until all cards are out
        for player in self.players:
            for hand in player.hands:
                handcount+=1
                hand.giveCard(self.shoe.deal())
                hand.giveCard(self.shoe.deal())
        
        if handcount == 0:
            return

        #deal dealer hand
        self.dealer.hands= []
        self.dealer.hands.append(BlackJackHand(self.dealer,0))
        self.dealer.hands[0].giveCard(self.shoe.deal())
        self.dealer.hands[0].giveCard(self.shoe.deal(faceup=False))

        #check for dealer blackjack
        if self.dealer.hands[0].value == 21:
            self.dealer.hands[0].blackjack = True
            for player in self.players:
                for hand in player.hands:
                    hand.done = True
                    if hand.value == 21:
                        hand.blackjack = True
                    else:
                        hand.lost = True
        else:
            for player in self.players:
                for hand in player.hands:
                    if hand.value == 21:
                        hand.done = True
                        hand.blackjack = True


        #play hands
        for player in self.players:
            player.playHands()
        

        #show dealer down card and adjust count
        val = valueDict[self.dealer.hands[0].cards[1][0]]
        if val >= 10:
            self.shoe.runningCount -=1
        elif val <= 6:
            self.shoe.runningCount +=1

        self.playDealerHand()
        dealer_line = "Dealer: " + self.dealer.hands[0].cards[0] + " |" 
        for card in self.dealer.hands[0].cards[1:]:
            dealer_line += " " + card
        dealer_line += " [{0}]".format(self.dealer.hands[0].value)

        #print(dealer_line)


        #check for winners
        for player in self.players:
            for hand in player.hands:
                av = 0
                if hand.lost:
                    av = -1 * hand.bet
                    pass
                elif self.dealer.hands[0].lost: #dealer bust
                    av = hand.bet
                    player.stack += hand.bet * 2
                elif hand.blackjack and not self.dealer.hands[0].blackjack:
                    av = hand.bet * self.blackjackpays
                    player.stack += hand.bet+ (hand.bet * self.blackjackpays)
                elif hand.value > self.dealer.hands[0].value: 
                    av = hand.bet
                    player.stack += hand.bet * 2
                elif hand.value == self.dealer.hands[0].value: #push
                    av = 0
                    player.stack += hand.bet
                elif hand.value < self.dealer.hands[0].value:
                    av = -1 * hand.bet
                    pass
                else:
                    player.av += av
                    hand.line +=" [{0}] av: {1}".format(hand.value, av)
                    print("WHAT? " + hand.line)
                    pass


                bin = self.getCountBin(startingTrueCount)
                upbin = self.getUpBin(self.dealer.hands[0].cards[0])

                if self.dealer.hands[0].lost:
                    upbin.dealerBust +=1
                    bin.dealerBust += 1

                player.av += av
                player.totalHands +=1

                bin.total +=1
                bin.av += av
                bin.bets += hand.bet

                upbin.total +=1 
                upbin.av += av 
                upbin.bets += hand.bet


                if av > 0:
                    player.handsWon += 1
                    bin.win +=1
                    upbin.win +=1
                elif av == 0:
                    player.handsPushed += 1
                    bin.push +=1
                    upbin.push +=1
                else:
                    player.handsLost += 1
                    bin.lose +=1
                    upbin.lose +=1

                if hand.blackjack:
                    bin.blackjack +=1
                    upbin.blackjack +=1
                
                for card in hand.cards:
                    self.shoe.discard(card)


                #hand.line +=" [{0}] av: {1}".format(hand.value, av)
                #if player.showHands:
                    #print(hand.line)
        
        for card in self.dealer.hands[0].cards: 
            self.shoe.discard(card)

        #check for shoe shuffle
        if self.shoe.shuffleReady:
            self.shoe.reset()


    

