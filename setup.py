import setuptools




with open("README.rst", "r") as fh:
    long_description = fh.read()

setuptools.setup(
     name='BlackJackSim',
     version="0.0.1",
     author="Jason Berger",
     author_email="berge472@gmail.com",
     description="blackjack sim engine",
     long_description=long_description,
     long_description_content_type="text/x-rst",
     scripts=['BlackJackSim/blackjack-test'],
     url="",
     packages=setuptools.find_packages(),
     install_requires=[
        'update_notipy'
     ],
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
 )
