#!/usr/bin/env python3

import BlackJackSim

def main():
    game = BlackJackSim.BlackJackGame(num_decks= 6, blackjackpays=1.5, penetration=0.75 )
    game.minbet = 10
    player1 = BlackJackSim.BlackJackPlayer("p1" ,showHands=False)
    player2 = BlackJackSim.BlackJackPlayer("p2",showHands=False)
    player3 = BlackJackSim.BlackJackPlayer("p3",showHands=False)
    hero = BlackJackSim.BlackJackPlayer("hero")

    #based on true count, play multiple hands per round and vary bets
    hero.strategy.addAdjustment(-7,0,0)
    hero.strategy.addAdjustment(-6,0,0)
    hero.strategy.addAdjustment(-5,0,0)
    hero.strategy.addAdjustment(-4,0,0)
    hero.strategy.addAdjustment(-3,0,0)
    hero.strategy.addAdjustment(-2,0,25)
    hero.strategy.addAdjustment(-1,1,25)
    hero.strategy.addAdjustment(0,1,50)
    hero.strategy.addAdjustment(1,1,50)
    hero.strategy.addAdjustment(2,2,100)
    hero.strategy.addAdjustment(3,2,200)
    hero.strategy.addAdjustment(4,2,400)
    hero.strategy.addAdjustment(5,2,400)
    hero.strategy.addAdjustment(6,2,400)
    hero.strategy.addAdjustment(7,2,400)
    hero.strategy.addAdjustment(8,2,400)
    hero.strategy.addAdjustment(9,2,400)

    game.addPlayer(player1)
    game.addPlayer(player2)
    game.addPlayer(player3)
    game.addPlayer(hero)



    for i in range(100000):
        game.playRound()

        if(i % 10000 == 0):
            #clear console
            print("\033c")
            hero.printStats()
            player1.printStats()


    game.printBins()

if __name__ == "__main__":
    main()
